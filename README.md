# Coding Challenge

## Available Scripts
In the project directory, you can run:

- **`yarn start`**
Runs the app in dev mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

- **`yarn test`**
Runs the test suite.

- **`yarn build`**
Builds the app for production to the build folder.

- **`yarn now-serve`**
Runs the app in production mode.
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

## Code Arquitecture
The code is distributed in components, containers and modules
```
src
├── components
│   └── ...
├── containers
│   ├── app
│   └── ...
└── modules
    ├── actions.js
    └── reducers.js
```

### Components
The components are small standalone "widgets" that are used by the containers or other components

### Containers
The containers have access to Redux state and to the actions.
The container `app` does all the routing to the other containers.

### Modules
Here is where the state and logic of the application resides.
Both actions and reducers can be splited into domain specific files when the application grows.

import {
  SET_SORTING_METHOD,
  SET_PLAYING_SONG,
  SET_SEARCHING_TERMS,
  SET_SONGS,
  CLEAR
} from './actions'

const initialState = {
  searchingTerms: '',
  list: [],
  playingSong: null,
  sortMethod: 'default',
  sortedList: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SEARCHING_TERMS:
      return {
        ...state,
        searchingTerms: action.data
      }
    case SET_SONGS:
      return {
        ...state,
        list: action.data.list,
        sortedList: action.data.sortedList
      }
    case CLEAR:
      return {
        ...state,
        list: [],
        sortedList: []
      }
    case SET_PLAYING_SONG:
      return {
        ...state,
        playingSong: action.data
      }
    case SET_SORTING_METHOD:
      return {
        ...state,
        sortMethod: action.data.sortMethod,
        sortedList: action.data.sortedList
      }
    default:
      return state
  }
}

import { combineReducers } from 'redux'
import songList from './reducers'

export default combineReducers({
  songList
})

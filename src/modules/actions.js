import fetchJsonp from 'fetch-jsonp'

export const CLEAR = 'list/CLEAR'
export const SET_SONGS = 'list/SET_SONGS'

export const millisToMinutesAndSeconds = (millis) => {
  var minutes = Math.floor(millis / 60000)
  var seconds = ((millis % 60000) / 1000).toFixed(0)
  return (minutes < 10 ? '0' : '') + minutes + ':' + (seconds < 10 ? '0' : '') + seconds
}

export const formatDate = (dateString) => {
  var d = new Date(dateString)
  return d.toDateString()
}

export const getSortedSongs = (songs, sorting) => {
  switch (sorting) {
    case 'primaryGenreName':
      return songs.slice().sort((a, b) => a[sorting] > b[sorting] ? 1 : -1)
    case 'trackTimeMillis':
    case 'trackPrice':
      return songs.slice().sort((a, b) => a[sorting] - b[sorting])
    case 'default':
    default:
      return songs.slice()
  }
}

export const searchSongs = (data, sorting) => {
  return dispatch => {
    if (data) {
      fetchJsonp(`https://itunes.apple.com/search?term=${encodeURIComponent(data)}&entity=song,`, {
        'credentials': 'include',
        'headers': {},
        'referrerPolicy': 'no-referrer-when-downgrade',
        'mode': 'no-cors',
        'method': 'GET' })
        .then((response) => response.json())
        .then((result) => {
          const songs = result.results.map(item => ({
            ...item,
            formatedReleaseDate: formatDate(item.releaseDate),
            formatedTrackTime: millisToMinutesAndSeconds(item.trackTimeMillis)
          }))
          dispatch({
            type: SET_SONGS,
            data: {
              list: songs,
              sortedList: getSortedSongs(songs, sorting)
            }
          })
        })
    } else {
      dispatch({
        type: CLEAR
      })
    }
  }
}

export const SET_SEARCHING_TERMS = 'list/SET_SEARCHING_TERMS'

export const setSearchingTerms = (data) => {
  return dispatch => {
    dispatch({
      type: SET_SEARCHING_TERMS,
      data: data
    })
  }
}

export const SET_PLAYING_SONG = 'list/SET_PLAYING_SONG'

export const setPlayinSong = (index) => {
  return dispatch => {
    dispatch({
      type: SET_PLAYING_SONG,
      data: index
    })
  }
}

export const nextSong = (currSong) => {
  return dispatch => {
    dispatch({
      type: SET_PLAYING_SONG,
      data: currSong + 1
    })
  }
}

export const previousSong = (currSong) => {
  return dispatch => {
    dispatch({
      type: SET_PLAYING_SONG,
      data: (currSong || 1) - 1 // never get below index 0
    })
  }
}

export const SET_SORTING_METHOD = 'list/SET_SORTING_METHOD'

export const setSortingMethod = (songs, sorting) => {
  return dispatch => {
    dispatch({
      type: SET_SORTING_METHOD,
      data: {
        sortMethod: sorting,
        sortedList: getSortedSongs(songs, sorting)
      }
    })
  }
}

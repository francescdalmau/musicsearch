import React from 'react'
import ListItem from '../ListItem'
import styles from './styles.module.css'

const PopulatedList = props => (
  <div className={styles.populatedList}>
    {
      props.items.map((item, index) => <ListItem
        key={index}
        item={item}
        onSongClick={() => props.onSongClick(index)}
      />)
    }
  </div>
)

export default PopulatedList

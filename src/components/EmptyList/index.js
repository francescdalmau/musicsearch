import React from 'react'
import styles from './styles.module.css'

const EmptyList = () => (
  <div className={styles.emptylist}>
    <div>Please enter a song name, </div>
    <div>an artist, a genre, ...</div>
    <div>e.g. Adele</div>
  </div>
)

export default EmptyList

import React from 'react'
import styles from './styles.module.css'

const ListItem = props => {
  const { artworkUrl100, trackName, artistName, collectionName, formatedReleaseDate, formatedTrackTime, primaryGenreName, trackPrice } = props.item

  return (
    <div className={styles.listItem} onClick={() => props.onSongClick()} >
      <img src={artworkUrl100.replace('100x100', '480x480')} className={styles.listItem_artwork} alt='' />
      <div className={styles.listItem_info}>
        <div className={styles.listItem_basicInfo}>
          <div className={styles.listItem_trackName}>{trackName}</div>
          <div className={styles.listItem_artistName}>{artistName}</div>
        </div>
        <div className={styles.listItem_expandedInfo}>
          <div className={styles.listItem_collectionName}><span className={styles['listItem_desc-key']}>Album:</span> <span className={styles['listItem_desc-value']}>{collectionName}</span></div>
          <div className={styles.listItem_releaseDate}><span className={styles['listItem_desc-key']}>Release:</span> <span className={styles['listItem_desc-value']}>{formatedReleaseDate}</span></div>
          <div className={styles.listItem_trackTimeMillis}><span className={styles['listItem_desc-key']}>Length:</span> <span className={styles['listItem_desc-value']}>{formatedTrackTime}</span></div>
          <div className={styles.listItem_primaryGenreName}><span className={styles['listItem_desc-key']}>Genre:</span> <span className={styles['listItem_desc-value']}>{primaryGenreName}</span></div>
          <div className={styles.listItem_trackPrice}><span className={styles['listItem_desc-key']}>Price:</span> <span className={styles['listItem_desc-value']}>${trackPrice}</span></div>
        </div>
      </div>
    </div>
  )
}

export default ListItem

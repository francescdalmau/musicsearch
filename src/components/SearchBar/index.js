import React, { Component } from 'react'
import styles from './styles.module.css'

class SearchBar extends Component {
  setSearchingTerms = ev => {
    this.props.setSearchingTerms(ev.target.value)
  };

  searchSongsKeyPress = ev => {
    if (ev.key === 'Enter') {
      this.props.searchSongs(this.props.searchingTerms, this.props.sortMethod)
    }
  };

  searchSongsOnClick = () => {
    this.props.searchSongs(this.props.searchingTerms, this.props.sortMethod)
  }

  setSortingMethod = ev => {
    this.props.setSortingMethod(this.props.list, ev.target.value)
  };

  render () {
    let { searchingTerms, sortMethod } = this.props
    return (
      <div className={styles.searchBar}>
        <input
          type='text'
          placeholder='Enter song, artist, genre, ...'
          className={styles.search_input}
          value={searchingTerms || ''}
          onChange={this.setSearchingTerms}
          onKeyPress={this.searchSongsKeyPress}
        />
        <span
          className={styles.search_button}
          onClick={this.searchSongsOnClick}
        >
          SEARCH
        </span>
        <div className={styles.search_sort}>
          <label htmlFor='select1'>Sort by: </label>
          <select
            value={sortMethod}
            onChange={this.setSortingMethod}
            className={styles.sort_select}
          >
            <option value='default'>Default</option>
            <option value='trackTimeMillis'>Length</option>
            <option value='primaryGenreName'>Genre</option>
            <option value='trackPrice'>Price</option>
          </select>
        </div>
      </div>
    )
  }
}

export default SearchBar

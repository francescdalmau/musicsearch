import React from 'react'
import { bindActionCreators } from 'redux'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import Share from '../../components/Share'

import {
  previousSong,
  nextSong
} from '../../modules/actions'

import styles from './styles.module.css'

class Favorites extends React.Component {
  constructor (props) {
    super(props)
    if (this.props.songList.playingSong === null) {
      this.props.backToList()
    }
  }

  backToList = () => this.props.backToList()

  previousSong = () => {
    this.props.previousSong(this.props.songList.playingSong)
  }

  nextSong = () => {
    this.props.nextSong(this.props.songList.playingSong)
  }

  render () {
    const index = this.props.songList.playingSong
    const song = this.props.songList.sortedList[index]
    const { trackName, artistName, artworkUrl100, previewUrl } = song
    if (!song) {
      return null
    }
    return (
      <div>
        <div className={styles['player-header']}>
          <span className={styles['player-header_back']} onClick={this.backToList}>🡠</span>
          Player
        </div>
        <div className={styles['player-mainbody']}>
          <div className={styles['player-info']}>
            <div className={styles['player-trackName']}>{trackName}</div>
            <div className={styles['player-artistName']}>{artistName}</div>
          </div>
          <div><img src={artworkUrl100.replace('100x100', '480x480')} alt='' /></div>
          <audio className={styles['player-audio']} key={index} autoPlay controls preload='none'>
            <source src={previewUrl} type='audio/mp4' />
            <p>Your browser does not support HTML5 audio.</p>
          </audio>

          <div className={styles['player-controls']}>
            <div
              className={`${styles['player-control_button']} ${styles['player-control_prev']}`}
              onClick={this.previousSong}>
              <span className={styles['player-control_label']}>&lt;</span>
            </div>
            <div
              className={`${styles['player-control_button']} ${styles['player-control_next']}`}
              onClick={this.nextSong}>
              <span className={styles['player-control_label']}>&gt;</span>
            </div>
          </div>
          <Share />
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ songList }) => ({
  songList
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      previousSong,
      nextSong,
      backToList: () => push('/')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Favorites)

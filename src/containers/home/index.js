import React, { Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import {
  setSortingMethod,
  setPlayinSong,
  searchSongs,
  setSearchingTerms
} from '../../modules/actions'
import PopulatedList from '../../components/PopulatedList'
import EmptyList from '../../components/EmptyList'
import SearchBar from '../../components/SearchBar'

import styles from './styles.module.css'

class Home extends React.Component {
  componentDidMount () {
    this.props.searchSongs(this.props.songList.searchingTerms, this.props.songList.sortMethod)
  }

  playSong = (index) => {
    this.props.setPlayinSong(index)
    this.props.goToPlayer()
  }

  render () {
    let { setSearchingTerms, searchSongs, setSortingMethod, songList } = this.props
    let { searchingTerms, sortMethod, list, sortedList } = songList
    return (
      <Fragment>
        <div className={styles.header}>
          My Music
        </div>
        <div className={styles.mainbody}>
          <SearchBar
            setSearchingTerms={setSearchingTerms}
            searchSongs={searchSongs}
            setSortingMethod={setSortingMethod}
            searchingTerms={searchingTerms}
            sortMethod={sortMethod}
            list={list}
          />
          {
            list.length
              ? <PopulatedList
                items={sortedList}
                onSongClick={this.playSong}
              />
              : <EmptyList />
          }
        </div>

      </Fragment>
    )
  }
}

const mapStateToProps = ({ songList }) => ({
  songList
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setSortingMethod,
      setPlayinSong,
      searchSongs,
      setSearchingTerms,
      goToPlayer: () => push('/player')
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)

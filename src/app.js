import React, { Fragment } from 'react'
import { Route } from 'react-router-dom'
import Home from './containers/home'
import Player from './containers/player'

const App = () => (
  <Fragment>
    <Route exact path='/' component={Home} />
    <Route exact path='/player' component={Player} />
  </Fragment>
)

export default App

import * as actions from '../../modules/actions'

describe('Actions', () => {
  describe('#millisToMinutesAndSeconds', () => {
    test('returns the time in minutes and seconds', () => {
      expect(actions.millisToMinutesAndSeconds(295502)).toBe('04:56')
      expect(actions.millisToMinutesAndSeconds(283960)).toBe('04:44')
      expect(actions.millisToMinutesAndSeconds(228293)).toBe('03:48')
    })
  })

  describe('#formatDate', () => {
    test('returns the formated date', () => {
      expect(actions.formatDate('2015-10-23T07:00:00Z')).toBe('Fri Oct 23 2015')
      expect(actions.formatDate('2011-01-24T08:00:00Z')).toBe('Mon Jan 24 2011')
      expect(actions.formatDate('2010-11-29T08:00:00Z')).toBe('Mon Nov 29 2010')
    })
  })
})

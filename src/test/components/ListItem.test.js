import React from 'react'
import ListItem from '../../components/ListItem'
import renderer from 'react-test-renderer'

describe('Components', () => {
  describe('ListItem', () => {
    it('renders correctly', () => {
      const item = {
        artworkUrl100: 'fakeUrl',
        trackName: 'fakeTrackName',
        artistName: 'fakeArtistName',
        collectionName: 'fakeCollectionName',
        formatedReleaseDate: 'fakeFormatedReleaseDate',
        formatedTrackTime: 'fakeFormatedTrackTime',
        primaryGenreName: 'fakePrimaryGenreName',
        trackPrice: 'fakeTrackPrice'
      }
      const tree = renderer
        .create(<ListItem
          item={item}
        />)
        .toJSON()
      expect(tree).toMatchSnapshot()
    })
  })
})

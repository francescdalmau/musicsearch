import React from 'react'
import EmptyList from '../../components/EmptyList'
import renderer from 'react-test-renderer'

describe('Components', () => {
  describe('EmptyList', () => {
    it('renders correctly', () => {
      const tree = renderer.create(<EmptyList />).toJSON()
      expect(tree).toMatchSnapshot()
    })
  })
})

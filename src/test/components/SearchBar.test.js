import React from 'react'
import SearchBar from '../../components/SearchBar'
import renderer from 'react-test-renderer'

describe('Components', () => {
  describe('SearchBar', () => {
    it('renders correctly with default values', () => {
      const props = {
        searchingTerms: '',
        sortMethod: 'default'
      }

      const tree = renderer.create(<SearchBar {...props} />).toJSON()
      expect(tree).toMatchSnapshot()
    })

    it('renders correctly with searching terms', () => {
      const props = {
        searchingTerms: 'fakeSearchingTerms',
        sortMethod: 'default'
      }

      const tree = renderer.create(<SearchBar {...props} />).toJSON()
      expect(tree).toMatchSnapshot()
    })

    it('renders correctly with sort method', () => {
      const props = {
        searchingTerms: 'fakeSearchingTerms',
        sortMethod: 'trackTimeMillis'
      }

      const tree = renderer.create(<SearchBar {...props} />).toJSON()
      expect(tree).toMatchSnapshot()
    })

    it('renders correctly without sort method', () => {
      const props = {
        searchingTerms: 'fakeSearchingTerms',
        sortMethod: null
      }

      const tree = renderer.create(<SearchBar {...props} />).toJSON()
      expect(tree).toMatchSnapshot()
    })
  })
})
